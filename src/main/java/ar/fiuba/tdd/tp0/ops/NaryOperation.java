package ar.fiuba.tdd.tp0.ops;

import java.util.EmptyStackException;
import java.util.Stack;
import java.util.function.Function;

/**
 * Created by ale on 9/9/15.
 */
public class NaryOperation implements Operator {

    private final Function<Float[], Float> func;
    private final int nuOperands;

    public NaryOperation(int nuOperands, Function<Float[], Float> func) {
        this.func = func;
        this.nuOperands = nuOperands;
    }

    @Override
    public Float compute(Stack<Float> rpnStack) {
        Float[] params = new Float[nuOperands];
        try {
            params[0] = rpnStack.pop();
        } catch (EmptyStackException e1) {
            throw new IllegalArgumentException();
        }
        try {
            for (int i = 1; i < nuOperands; ++i) {
                params[i] = rpnStack.pop();
            }
            return func.apply(params);
        } catch (EmptyStackException e1) {
            return params[0];
        }
    }
}