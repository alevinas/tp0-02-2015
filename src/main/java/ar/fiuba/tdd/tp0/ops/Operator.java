package ar.fiuba.tdd.tp0.ops;

import java.util.Stack;

/**
 * Created by ale on 9/7/15.
 */
public interface Operator {

    Float compute(Stack<Float> floatStack);
}
