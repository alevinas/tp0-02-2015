package ar.fiuba.tdd.tp0.ops;

import java.util.EmptyStackException;
import java.util.Stack;
import java.util.function.BinaryOperator;

/**
 * Created by ale on 9/9/15.
 */
public class BinaryOperation implements Operator {

    BinaryOperator<Float> func;

    public BinaryOperation(BinaryOperator<Float> func) {
        this.func = func;
    }

    @Override
    public Float compute(Stack<Float> rpnStack) {
        try {
            float a1 = rpnStack.pop();
            float a2 = rpnStack.pop();
            return func.apply(a1, a2);

        } catch (EmptyStackException e1) { //No se me ocurre como manejar cuando la pila esta vacia
            throw new IllegalArgumentException();
        }
    }
}
