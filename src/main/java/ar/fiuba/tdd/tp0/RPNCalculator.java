package ar.fiuba.tdd.tp0;

import ar.fiuba.tdd.tp0.ops.BinaryOperation;
import ar.fiuba.tdd.tp0.ops.NaryOperation;
import ar.fiuba.tdd.tp0.ops.Operator;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Stack;
import java.util.function.BinaryOperator;
import java.util.function.Function;

public class RPNCalculator {

    private final Stack<Float> rpnStack = new Stack<>();
    Map<String, Operator> operators = new HashMap<String, Operator>();


    public RPNCalculator() {
        this.fillBasicOperations();
        //Aca agregaria mas operaciones con register[Bi/N]naryOperation(...)
        this.registerBinaryOperator("MAX", Math::max);

        this.registerNaryOperator("SIXMOD", 6,
                floats -> floats[5] / floats[4] % floats[3] / floats[2] / floats[1] / floats[0]);

    }

    public float eval(final String expression) throws IllegalArgumentException {

        Optional<String> maybeExpression = Optional.ofNullable(expression);

        maybeExpression.orElseThrow(IllegalArgumentException::new);

        String[] todoString = maybeExpression.get().split("\\s+");

        for (String elem : todoString) {
            try {
                rpnStack.push(new Float(elem)); //No se me ocurre como crear Numeros por una parte y operaciones por otra sin este try

            } catch (NumberFormatException e) {
                Optional<Operator> maybeOp = Optional.ofNullable(
                        operators.get(elem));

                maybeOp.orElseThrow(IllegalArgumentException::new); //Si la operacion no estaba en el hashmap, sale exception

                rpnStack.push(maybeOp.get().compute(rpnStack));
            }
        }
        return rpnStack.pop();


    }

    public void fillBasicOperations() {
        operators.put("+", new BinaryOperation((a1, a2) -> a1 + a2));
        operators.put("-", new BinaryOperation((a1, a2) -> a2 - a1));
        operators.put("*", new BinaryOperation((a1, a2) -> a1 * a2));
        operators.put("/", new BinaryOperation((a1, a2) -> a2 / a1));
        operators.put("MOD", new BinaryOperation((a1, a2) -> a2 % a1));

        operators.put("++", new NaryOperation(4,
                floats -> floats[3] + floats[2] + floats[1] + floats[0]));

        operators.put("--", new NaryOperation(4,
                floats -> floats[3] - floats[2] - floats[1] - floats[0]));

        operators.put("**", new NaryOperation(4,
                floats -> floats[3] * floats[2] * floats[1] * floats[0]));

        operators.put("//", new NaryOperation(4,
                floats -> floats[3] / floats[2] / floats[1] / floats[0]));
    }

    public void registerBinaryOperator(String name, BinaryOperator<Float> func) {
        operators.put(name, new BinaryOperation(func));
    }

    public void registerNaryOperator(String name, int nuOperands, Function<Float[], Float> func) {
        operators.put(name, new NaryOperation(nuOperands, func));
    }

}
